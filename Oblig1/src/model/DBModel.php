<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
		if ($db) 
		{
			$this->db = $db;
		}
		else				// Create PDO connection
		{
			$this->db = new PDO('mysql:host=localhost;dbname=task3db;charset=utf8',
			'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		try {
			$list = array();
			$stmt = $this->db->query('SELECT id, title, author, description FROM book ORDER BY id');
			foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $book) {
				$list[] = new Book($book['title'], $book['author'], $book['description'], $book['id']);
			}
			return $list;
			
		} catch (PDOException $e) {
			echo "Error, can not execute getBookList";
			echo $e->getMessage();							// Show message (should not show other then for testing)
		}
		
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		try {
			$id = intval($id);
			$stmt = $this->db->prepare('SELECT id, title, author, description FROM book WHERE id = :id');
			$stmt->execute(array('id' => $id));
			$book =	$stmt->fetch(PDO::FETCH_ASSOC);
			return new Book($book['title'], $book['author'], $book['description'], $book['id']);
			
		} catch (PDOException $e) {
			echo "Error, can not execute getBookById";
			echo $e->getMessage();
		}
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
		try {
			
			$stmt = $this->db->prepare('INSERT INTO book (title, author, description) VALUES(:title, :author, :description)');
			$stmt->bindParam(":title", $this->title);
			$stmt->bindParam(":author", $this->author);
			$stmt->bindParam(":description", $this->description);
			
			$book = $stmt->execute();
			$this->db->lastInsertId();
			
		} catch (PDOException $e) {
			echo "Error, can not execute addBook";
			echo $e->getMessage();
		}
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
	public function modifyBook($book)
    {
		$stmt = $this->db->prepare ('UPDATE book SET title = :title, author = :author, description = :description WHERE id = :id');
		$stmt->bindValue(':title', $title);
		$stmt->bindValue(':author', $author);
		$stmt->bindValue(':description', $description);
		$book = $stmt->execute(); 
		
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
	public function deleteBook($id)
    {
		$id = intval($id);
		$stmt = $this->db->prepare('DELETE FROM book WHERE id = :id');
		$stmt->execute(array('id' => $id));
		
    }
	
}

?>